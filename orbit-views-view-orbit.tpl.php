<?php

/**
 * Template file for Orbit slideshow.
 */

?>

<ul data-orbit data-options="<?php print $orbit_options; ?>">
  <?php foreach ($rows as $row): ?>
    <li><?php print $row; ?></li>
  <?php endforeach; ?>
</ul>
