<?php

/**
 * Views style plugin for Zurb Foundation Orbit slideshow.
 */

/**
 * Implements hook_views_plugins().
 */
function orbit_views_views_plugins() {
  return array(
    'style' => array(
      'views_orbit_orbit' => array(
        'title' => t('Orbit'),
        'help' => t('Display data in an Orbit slideshow.'),
        'handler' => 'orbit_views_plugin_style_orbit',
        'theme' => 'orbit_views_view_orbit',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses row plugin' => TRUE,
        'type' => 'normal',
        'event empty' => TRUE,
      ),
    ),
  );
}
