<?php

/**
 * @file
 * Contains the Orbit style plugin.
 */

/**
 * Orbit style plugin.
 */
class orbit_views_plugin_style_orbit extends views_plugin_style {
  
  function option_definition() {
    $options = parent::option_definition();
    
    $options['caption_field'] = array('default' => 'none');
    $options['animation'] = array('default' => 'fade');
    $options['timer_speed'] = array('default' => 10000);
    $options['pause_on_hover'] = array('default' => 'true');
    $options['resume_on_mouseout'] = array('default' => 'false');
    $options['slide_number'] = array('default' => 'true');
    $options['navigation_arrows'] = array('default' => 'true');
    $options['bullets'] = array('default' => 'true');
    
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    // Get all fields in this view.
    $handlers = $this->display->handler->get_handlers('field');
    
    // Create select list of fields in view.
    $fields = array('none' => 'None');
    foreach ($handlers as $field_id => $handler) {
      $fields[$field_id] = $handler->ui_name();
    }
    
    // Create an array for true and false values.
    $tf_options = array(
      'true' => t('True'),
      'false' => t('False'),
    );
    
    $form['caption_field'] = array(
      '#type' => 'select',
      '#title' => t('Caption field'),
      '#description' => t('Select a field to display as a caption if desired'),
      '#options' => $fields,
      '#default_value' => $this->options['caption_field'],
    );
    
    $form['animation'] = array(
      '#type' => 'select',
      '#title' => t('Animation'),
      '#description' => t('Sets the type of animation used for transitioning between slides'),
      '#options' => array(
        'slide' => t('Slide'),
        'fade' => t('Fade'),
      ),
      '#default_value' => $this->options['animation'],
    );
    
    $form['timer_speed'] = array(
      '#type' => 'textfield',
      '#title' => t('Timer speed'),
      '#description' => t('Sets the amount of time in milliseconds before transitioning a slide'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->options['timer_speed'],
    );
    
    $form['pause_on_hover'] = array(
      '#type' => 'select',
      '#title' => t('Pause on hover'),
      '#description' => t('Pauses on the current slide while hovering'),
      '#options' => $tf_options,
      '#default_value' => $this->options['pause_on_hover'],
    );
    
    $form['resume_on_mouseout'] = array(
      '#type' => 'select',
      '#title' => t('Resume on mouseout'),
      '#description' => t('If pause on hover is set to true, this setting resumes playback after mousing out of slide'),
      '#options' => $tf_options,
      '#default_value' => $this->options['resume_on_mouseout'],
    );
    
    $form['slide_number'] = array(
      '#type' => 'select',
      '#title' => t('Show slide number'),
      '#description' => t('Show current and total slide numbers'),
      '#options' => $tf_options,
      '#default_value' => $this->options['slide_number'],
    );
    
    $form['navigation_arrows'] = array(
      '#type' => 'select',
      '#title' => t('Navigation arrows'),
      '#description' => t('Display navigation arrows?'),
      '#options' => $tf_options,
      '#default_value' => $this->options['bullets'],
    );
    
    $form['bullets'] = array(
      '#type' => 'select',
      '#title' => t('Bullets'),
      '#description' => t('Does the slider have bullets visible?'),
      '#options' => $tf_options,
      '#default_value' => $this->options['bullets'],
    );
    
  }
  
  function pre_render($result) {
    parent::pre_render($result);
    
    // Wrap caption field in caption class.
    if ($this->options['caption_field'] != 'none') {
      foreach ($result as $key => $value) {
        $caption_field = 'field_' . $this->options['caption_field'];
        $result[$key]->{$caption_field}[0]['rendered']['#prefix'] = '<div class="orbit-caption">';
        $result[$key]->{$caption_field}[0]['rendered']['#suffix'] = '</div>';
      }
    }
    
  }
  
}
